var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');

var scrap = require('./scripts/scrap.js').scr;


/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'comp',
        debug    : false //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
    res.send('Welcome');
});


//RESTful route
var router = express.Router();


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

var curut = router.route('/item');


//show the CRUD interface | GET
curut.get(function(req,res,next){


    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query('SELECT * FROM hh_balrog_request',function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            res.render('item',{title:"REST Google Shopping",data:rows});

         });

    });

});
//post data to DB | POST
curut.post(function(req,res,next){

    //validation
    req.assert('Keyword','Keyword is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM hh_balrog_response WHERE request_id = (SELECT id from hh_balrog_request WHERE query_string = ? LIMIT 1) ",[req.body.Keyword], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }
                if(rows.length < 1 ){
                res.render('ebresult',{title : "No Result",req:req.body.Keyword});
            } else {

                res.render('tab',{data:rows});
        }

        });

     });

});

var curut3 = router.route('/keyword');


//show the CRUD interface | GET
curut3.get(function(req,res,next){


    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query('SELECT * FROM hh_balrog_request',function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            res.render('item',{title:"REST Google Shopping",data:rows});

         });

    });

});

curut3.put(function(req,res,next){
    var keyword = req.body.Keyword;
    //validation
    req.assert('Keyword','Keyword is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        console.log("error")
        res.status(422).json(errors);
        return;
    }

    //get data
    var data_req = {
    source : '',
    process : 'google',
    country :  null,
    query_string : keyword,
    insert_daytime : new Date(),
    process_id : '',
    status : '9',
  }
    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO hh_balrog_request(source_id, processor_id, country, query_string, insert_datetime, processed_datetime, status ) VALUES ( ?, ?, ?, ?, ?, ?, ?)", [data_req.source, data_req.process, data_req.country, data_req.query_string, data_req.insert_daytime, data_req.process_id, data_req.status], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }
            var y = scrap(keyword);
            res.send("Done, See Results or DB dumps")
        });

     });

});

var curut2 = router.route('/item/:item_query');

/*------------------------------------------------------
route.all is extremely useful. you can use it to do
stuffs for specific routes. for example you need to do
a validation everytime route /api/user/:user_id it hit.

remove curut2.all() if you dont want it
------------------------------------------------------*/
curut2.all(function(req,res,next){
    next();
});

//get data to update
curut2.get(function(req,res,next){

    var querystring = req.params.item_query;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM hh_balrog_response WHERE request_id = (SELECT id from hh_balrog_request WHERE query_string = ? LIMIT 1) ",[querystring],function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("Query Not found");

            res.render('tab',{title:"Results",data:rows});
        });

    });

});

//delete data
curut2.delete(function(req,res,next){

    var queryid = req.params.item_query;
     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM hh_balrog_request WHERE id = ?",[queryid], function(err, rows){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

            var query_res = conn.query("DELETE FROM hh_balrog_response WHERE request_id = ? ",[queryid], function(err, r){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

            });

             res.sendStatus(200);

        });
        //console.log(query.sql);

     });
});

//now we need to apply our router here
app.use('/api', router);

//start Server
var server = app.listen(3000,function(){

   console.log("Listening to port %s",server.address().port);

});
