var mysql      = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'comp'
});

function inserting_req_data(req){

connection.query("INSERT INTO hh_balrog_request(source_id, processor_id, country, query_string, insert_datetime, processed_datetime, status ) VALUES ( ?, ?, ?, ?, ?, ?, ?)", [req.source, req.process, req.country, req.query_string, req.insert_daytime, req.process_id, req.status], function(err, rows, fields) {
  if (!err)
    console.log('Success');
  else
    console.log(err);
});

};

function inserting_resp_data(req){

connection.query("INSERT INTO hh_balrog_response(request_id, product_url, image_url, insert_datetime, processed_datetime, status ) VALUES ( ?, ?, ?, ?, ?, ?)", [req.request_id, req.prod_url, req.img_url, req.insert_datetime, req.process_id, req.status], function(err, rows, fields) {
  if (!err) 
    console.log("success")
  else
    console.log(err);
});

};

function update(req1, req2){
  var pr = new Date();
connection.query("UPDATE hh_balrog_request SET country=?,processed_datetime =?,status=2  WHERE id = ?",[req1, pr, req2.request_id], function(err, rows, fields) {
  if (!err) 
    console.log("success")
  else
    console.log(err);
});

};
module.exports.update = update;
module.exports.inserting_req_data = inserting_req_data;
module.exports.inserting_resp_data = inserting_resp_data;