var mysql      = require('mysql');
var inserting_resp_data = require('./mysq.js').inserting_resp_data;

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'comp'
});

try {
    var Spooky = require('spooky');
} catch (e) {
    var Spooky = require('../lib/spooky');
}

function scr(req){

var spooky = new Spooky({
    child: {
        transport: 'http'
    },
    casper: {
        logLevel: 'debug',
        verbose: true
    }
}, function (err) {
    if (err) {
        e = new Error('Failed to initialize SpookyJS');
        e.details = err;
        throw e;
    }

    var links = [];
    var gallery = [];
    console.log("start scraping");

    spooky.start('https://www.google.com/shopping?hl=en',function(){
        // this.fill('form[action="/search"]', { q: req }, true);
    });
    spooky.then([{req: req}, function () {
        this.fill('form[action="/search"]', { q: req }, true);
    }]);
    
    spooky.then(function () {

        links = this.evaluate(function () {
            var link= document.querySelectorAll('h3.r a');
            return Array.prototype.map.call(link, function(e) {
                return e.getAttribute('href');
            });
        });

    });

    spooky.then(function () {
        gallery = this.evaluate(function () {
            var images= document.querySelectorAll('img._zyj');
            return Array.prototype.map.call(images, function(e) {
                return e.getAttribute('src');
            });
        });
    });

    

    spooky.then(function () {
        this.emit('hello',links, gallery);
    });
    spooky.run();
});

spooky.on('error', function (e, stack) {
    console.error(e);

    if (stack) {
        console.log(stack);
    }
});


spooky.on('hello', function (links, gallery) {
    connection.query("SELECT id from hh_balrog_request WHERE query_string = ? LIMIT 1", [req], function(err, rows, fields) {
      if (err)
        console.log(err);
    if (links.length == 0)
    {
        var req = {
            request_id : rows[0].id,
            prod_url : "n",
            img_url : "n",
            insert_datetime : new Date(),
            process_id :"google",
            status : '7'
        };
        inserting_resp_data(req);
        var res = "No results with GS"
    } else {

        console.log("gallery long:  " + gallery.length)
         var dat = {
            request_id : rows[0].id,
            prod_url : "n",
            img_url : "n",
            insert_datetime : new Date(),
            process_id :"google",
            status : '9'
        };
        for (var i = 0; i < links.length; i++) {
            dat.prod_url = "https://www.google.com/"+links[i];
            // dat.img_url = gallery[i];
            dat.insert_datetime = new Date();
            inserting_resp_data(dat);
          }
        var res = "Success, see db dumbs"

    }

        
});
    return res
});

spooky.on('log', function (log) {
    if (log.space === 'remote') {
        console.log(log.message.replace(/ \- .*/, ''));
    }
});


};

module.exports.scr = scr;